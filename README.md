# WorkHelp
懒人小工具：T4自动生成Model,Insert,Select,Delete以及导出Excel的方法
由于最近公司在用webform开发ERP，用到大量重复机械的代码，之前写了篇文章，懒人小工具：自动生成Model,Insert,Select,Delete以及导出Excel的方法,但是有人觉得这种方法很麻烦。其实我感觉确实是有点麻烦，麻烦在于字符串的拼接。
这种时候我想到了T4模板，用过EF的 DatabaseFirst自动生成实体的同学就明白，dbfirst 自带T4模板，之前我们在学习spring.net框架的时候，也有用过T4模板根据数映射到实体类自动创建仓储。T4模板其实还有很多应用的场景。
T4模板确实挺方便的，但是其实其中用过的原理和我之前做winform小工具差不多。都是根据数据字段和类型的映射生成实体。另外就是ado.net基础知识。

博客园地址：http://www.cnblogs.com/anyushengcms/p/7573289.html

github地址：https://github.com/Jimmey-Jiang